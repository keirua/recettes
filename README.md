# Recettes

Ce projet me sert principalement à garder une trace des recettes que j'ai réalisé. On peut parfois y trouver la recette originale (ainsi qu'un lien vers le site en question), ainsi que quelques unes de mes notes à ce sujet.
Ces recettes se trouvent dans les 3 répertoires "entrees", "plats" et "desserts".

Le répertoire "idees" me sert à noter des idées de recettes (que ce soit juste une idée de plat, ou bien toute une recette) que j'aimerais réaliser.

Enfin, le répertoire bière contient une liste des bières que j'ai goûtées récemment.