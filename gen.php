<?php
$recipeList = include ('recipeList.php');
$categoryList = array (
  null,
  array ('path' => 'entrees',  'title' => 'Entrée'),
  array ('path' => 'plats',    'title' => 'Plats'),
  array ('path' => 'desserts', 'title' => 'Desserts'),
  );

function printRecipe ($r){
  $result  = '';
  $result .= '# '.($r['title'])."\n";

  if (!empty ($r['url'])) {
    $result .= '['.$r['url'].']'.'('.$r['url'].')'."\n\n";
  }

  $result .= 'Pour '.($r['nbPersons'])."\n\n";

  if (!empty ($r['ingredients'])) {
    $result .= '## Ingrédients'."\n";
    $ingredientList = explode ('#!!#', $r['ingredients']);
    $ingredientImploded = "";
    if (count($ingredientList) > 0){
      $ingredientImploded = "* ".implode ("\n* ", $ingredientList)."\n";
    }
    $result .= $ingredientImploded."\n";
    // $result .= str_replace('#!!#', "\n", $r['ingredients'])."\n\n";
  }

  if (!empty ($r['instructions'])) {
    $result .= '## Instructions'."\n";
    $result .= ($r['instructions'])."\n";
  }

  return $result;
}


function sanitize($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}

function getFilename ($recipe){
  return sanitize ($recipe['title'], true).'.md';
}

function getPath ($recipe) {
  global $categoryList;

  $p = $categoryList[$recipe['category_id']]['path'];

  return './'.$p.'/';
}

function saveRecipe ($recipe){
    $path = getPath ($recipe);
    $filename = getFilename ($recipe);
    $content = printRecipe ($recipe);

    file_put_contents ($path.$filename, $content);
}

function generateLink ($r){
  $link = $r['url'];
  if ($r['done']) {
    $link = getPath ($r).getFilename($r);
  }

  return '[link]'.'('.$link.')';
}

function getRecipeSummary ($recipeList){
  global $categoryList;

  $result = '# Liste des recettes'."\n";
  $categories = array (1,2,3);

  foreach ($categories as $currCategory) {
    $recipeSubset = array_filter($recipeList, function ($r) use ($currCategory) {
      return $r['category_id'] == $currCategory;
    });
    
    if (count($recipeSubset) > 0){
      $result .= '## '.$categoryList[$currCategory]['title']."\n";

      foreach ($recipeSubset as $currRecipe) {
        if (!empty ($currRecipe['title']))
          $result .= '* '.$currRecipe['title'].' '.generateLink($currRecipe)."\n";
      }
      $result .= "\n";
    }

  }

  return $result;
}

$doneRecipes = array_filter($rsp_recipe, function ($r){
  return $r['done'] == 1;
});

foreach ($doneRecipes as $currRecipe) {
  saveRecipe ($currRecipe);
}

$todoRecipes = array_filter($rsp_recipe, function ($r){
  return $r['done'] == 0;
});

// echo getRecipeSummary ($todoRecipes);
file_put_contents("liste-fait.md",   getRecipeSummary ($doneRecipes));
file_put_contents("liste-a-tester.md", getRecipeSummary ($todoRecipes));